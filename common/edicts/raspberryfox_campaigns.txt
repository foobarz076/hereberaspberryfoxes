#################
# Campaigns
# shorter duration, cost energy
#################
@Edict1Cost = 20
@Edict2Cost = 25
@Edict3Cost = 30
@EdictDuration = -1
# Sets the cost for all campaigns with this variable
@campaignCost = 15
@hiveFood = 15
@campaignDuration = -1

raspberryfox_assistance_mega = {
	length = @EdictDuration
	icon = "GFX_edict_type_time"
	resources = {
		category = rare_edicts
		cost = {
		}
		upkeep = {
			sr_living_metal = 5
			multiplier = value:edict_size_effect
		}
	}
	modifier = {
		country_megastructure_build_cap_add = 1
		megastructure_build_speed_mult = 1
	}
	prerequisites = {
		"tech_mega_engineering"
	}
	ai_weight = {
		weight = 0
	}
}

@raspberryfox_cost = 5000
@raspberryfox_cost_rare = 500
@raspberryfox_maintenance = 5
@raspberryfox_maintenance_rare = 1
@raspberryfox_district_buildtime = 720
@raspberryfox_district_basic_units = 20
@raspberryfox_district_advanced_units = 10

district_raspberryfox_city = {
	base_buildtime = @raspberryfox_district_buildtime
	is_capped_by_modifier = no
	exempt_from_ai_planet_specialization = yes
	show_on_uncolonized = {
		always = no
	}
	potential = {
		exists = owner
		uses_district_set = raspberryfox
	}
	resources = {
		category = planet_districts
		cost = {
			minerals = @raspberryfox_cost
		}
		upkeep = {
			energy = @raspberryfox_maintenance
		}
	}
	planet_modifier = {
		planet_housing_add = 50
	}
	triggered_planet_modifier = {
		potential = {
			exists = owner
			owner = {
				is_raspberryfox_empire = yes
			}
		}
		modifier = {
			planet_housing_add = 50
			job_maintenance_drone_add = @raspberryfox_district_basic_units
			job_synapse_drone_add = @raspberryfox_district_advanced_units
		}
	}
	triggered_planet_modifier = {
		potential = {
			exists = owner
			owner = {
				has_active_tradition = tr_prosperity_public_works
			}
		}
		modifier = {
			planet_housing_add = 20
		}
	}
	triggered_planet_modifier = {
		potential = {
			exists = owner
			owner = {
				has_technology = tech_housing_1
			}
		}
		modifier = {
			planet_housing_add = 20
		}
	}
	triggered_planet_modifier = {
		potential = {
			exists = owner
			owner = {
				has_technology = tech_housing_2
				NOT = {
					has_valid_civic = civic_agrarian_idyll
				}
			}
		}
		modifier = {
			planet_housing_add = 20
		}
	}
	triggered_planet_modifier = {
		potential = {
			exists = owner
			owner = {
				AND = {
					is_raspberryfox_empire = yes
					has_technology = tech_housing_2
				}
			}
		}
		modifier = {
			planet_housing_add = 20
			job_maintenance_drone_add = @raspberryfox_district_basic_units
		}
	}
	triggered_planet_modifier = {
		potential = {
			exists = owner
			owner = {
				has_active_tradition = tr_prosperity_extended_hives
			}
		}
		modifier = {
			planet_housing_add = 20
		}
	}
	triggered_desc = {
		trigger = {
			exists = owner
			owner = {
				is_regular_empire = yes
			}
		}
		text = job_clerk_effect_desc
	}
}

district_raspberryfox_generator = {
	base_buildtime = @raspberryfox_district_buildtime
	is_capped_by_modifier = no
	show_on_uncolonized = {
		always = no
	}
	potential = {
		exists = owner
		uses_district_set = raspberryfox
	}
	resources = {
		category = planet_districts
		cost = {
			minerals = @raspberryfox_cost
		}
		upkeep = {
			energy = @raspberryfox_maintenance
		}
	}
	triggered_planet_modifier = {
		planet_housing_add = 20
	}
	triggered_planet_modifier = {
		potential = {
			exists = owner
			owner = {
				is_raspberryfox_empire = yes
			}
		}
		planet_housing_add = 20
		job_technician_drone_add = @raspberryfox_district_basic_units
		job_mining_drone_add = @raspberryfox_district_basic_units
	}
	triggered_desc = {
		trigger = {
			planet = {
				has_deposit = d_arcane_generator
				NOT = {
					has_district = district_raspberryfox_generator
				}
			}
		}
		text = arcane_generator_upkeep_desc
	}
	triggered_desc = {
		trigger = {
			exists = owner
			owner = {
				is_gestalt = yes
			}
		}
		text = job_technician_drone_effect_desc
	}
	triggered_desc = {
		trigger = {
			exists = owner
			owner = {
				is_gestalt = yes
			}
		}
		text = job_mining_drone_effect_desc
	}
}

district_raspberryfox_science = {
	base_buildtime = @raspberryfox_district_buildtime
	is_capped_by_modifier = no
	show_on_uncolonized = {
		uses_district_set = raspberryfox
	}
	potential = {
		uses_district_set = raspberryfox
	}
	resources = {
		category = planet_districts
		cost = {
			minerals = @raspberryfox_cost
		}
		upkeep = {
			energy = @raspberryfox_maintenance
		}
	}
	triggered_planet_modifier = {
		planet_housing_add = 20
	}
	triggered_planet_modifier = {
		potential = {
			exists = owner
			owner = {
				is_regular_empire = yes
			}
		}
		modifier = {
			job_researcher_add = 20
		}
	}
	triggered_planet_modifier = {
		potential = {
			exists = owner
			owner = {
				is_raspberryfox_empire = yes
			}
		}
		modifier = {
			job_brain_drone_add = @raspberryfox_district_advanced_units
		}
	}

	triggered_desc = {
		trigger = {
			planet = {
				has_deposit = d_arcane_generator
				NOT = {
					has_district = district_raspberryfox_science
				}
			}
		}
		text = arcane_generator_upkeep_desc
	}
	triggered_desc = {
		trigger = {
			exists = owner
			owner = {
				is_regular_empire = yes
			}
		}
		text = job_researcher_effect_desc
	}
	triggered_desc = {
		trigger = {
			exists = owner
			owner = {
				is_raspberryfox_empire = yes
			}
		}
		text = job_brain_drone_effect_desc
	}
}

district_raspberryfox_farming = {
	base_buildtime = @raspberryfox_district_buildtime
	is_capped_by_modifier = no
	show_on_uncolonized = {
		uses_district_set = raspberryfox
	}
	potential = {
		uses_district_set = raspberryfox
		NOT = {
			AND = {
				owner = {
					is_ai = yes
				}
				owner = {
					country_uses_food = no
				}
			}
		}
	}
	resources = {
		category = planet_districts
		cost = {
			minerals = @raspberryfox_cost
		}
		upkeep = {
			energy = @raspberryfox_maintenance
		}
	}
	# triggered for tooltip formatting purposes
	triggered_planet_modifier = {
		modifier = {
			planet_housing_add = 20
		}
	}
	triggered_planet_modifier = {
		potential = {
			exists = owner
			owner = {
				is_raspberryfox_empire = yes
			}
		}
		modifier = {
			planet_housing_add = 10
			job_agri_drone_add = @raspberryfox_district_basic_units
		}
	}
	triggered_planet_modifier = {
		potential = {
			exists = owner
			owner = {
				has_valid_civic = civic_agrarian_idyll
			}
		}
		modifier = {
			planet_housing_add = 10
		}
	}
	triggered_planet_modifier = {
		potential = {
			exists = owner
			owner = {
				has_technology = tech_housing_agrarian_idyll
				has_valid_civic = civic_agrarian_idyll
			}
		}
		modifier = {
			planet_housing_add = 10
		}
	}
	triggered_desc = {
		trigger = {
			exists = owner
			owner = {
				is_gestalt = yes
			}
		}
		text = job_agri_drone_effect_desc
	}
	triggered_desc = {
		trigger = {
			exists = owner
			owner = {
				is_gestalt = no
				is_fallen_empire_spiritualist = no
			}
		}
		text = job_farmer_effect_desc
	}
	triggered_desc = {
		trigger = {
			exists = owner
			owner = {
				is_fallen_empire_spiritualist = yes
			}
		}
		text = job_fe_acolyte_farm_effect_desc
	}
}

district_raspberryfox_industrial = {
	base_buildtime = @raspberryfox_district_buildtime
	is_capped_by_modifier = no
	show_on_uncolonized = {
		uses_district_set = raspberryfox
	}
	potential = {
		uses_district_set = raspberryfox
	}
	resources = {
		category = planet_districts
		cost = {
			minerals = @raspberryfox_cost
		}
		upkeep = {
			energy = @raspberryfox_maintenance
		}
		upkeep = {
			trigger = {
				exists = owner
				owner = {
					has_edict = industrial_maintenance
				}
			}
			energy = 4
		}
	}
	triggered_planet_modifier = {
		planet_housing_add = 20
	}
	triggered_planet_modifier = {
		potential = {
			exists = owner
			owner = {
				is_raspberryfox_empire = yes
				is_catalytic_empire = no
			}
		}
		modifier = {
			job_alloy_drone_add = @raspberryfox_district_advanced_units
		}
	}
	triggered_desc = {
		trigger = {
			exists = owner
			owner = {
				is_raspberryfox_empire = yes
				is_catalytic_empire = no
			}
		}
		text = job_alloy_drone_effect_desc
	}
}

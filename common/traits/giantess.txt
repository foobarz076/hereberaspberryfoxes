trait_giantess_potential = {
	cost = 0
	initial = no
	randomized = no
	icon = "gfx/interface/icons/traits/leader_traits/trait_ruler_world_shaper.dds"
	
	modifier ={
		# army_damage_mult = 4.0
		# planet_jobs_worker_produces_mult = 2.0
		# ship_hull_mult = 5
		# ship_hull_regen_add_perc = 5
		# ship_armor_regen_add_perc = 5
		# # planet_jobs_engineering_research_produces_mult = 2
		# # planet_jobs_physics_research_produces_mult = 2
		# # planet_jobs_society_research_produces_mult = 2
		# pop_housing_usage_mult = 2.0
		
	}

	allowed_archetypes = {
		RaspberryFox
	}

	species_potential_add = {
		always = no
	}
	species_possible_remove = {
		always = no
	}
	species_possible_merge_add = {
		always = no
	}
}
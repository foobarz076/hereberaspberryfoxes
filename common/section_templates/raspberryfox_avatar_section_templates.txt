ship_section_template = {
	key = "raspberryfox_avatar_section_new"
	ship_size = raspberryfox_avatar
	fits_on_slot = mid
	entity = "shroud_creature_corrupted_01_entity"
	icon = "GFX_ship_part_core_mid"
	
	component_slot = {
		name = "MEDIUM_GUN_01"
		template = "medium_turret"
		locatorname = "root"
	}
	component_slot = {
		name = "MEDIUM_GUN_02"
		template = "medium_turret"
		locatorname = "root"
	}
	component_slot = {
		name = "MEDIUM_GUN_03"
		template = "medium_turret"
		locatorname = "root"
	}
	component_slot = {
		name = "MEDIUM_GUN_04"
		template = "medium_turret"
		locatorname = "root"
	}
	component_slot = {
		name = "MEDIUM_GUN_05"
		template = "medium_turret"
		locatorname = "root"
	}
	component_slot = {
		name = "MEDIUM_GUN_06"
		template = "medium_turret"
		locatorname = "root"
	}
	component_slot = {
		name = "MEDIUM_GUN_07"
		template = "medium_turret"
		locatorname = "root"
	}
	component_slot = {
		name = "MEDIUM_GUN_08"
		template = "medium_turret"
		locatorname = "root"
	}
	component_slot = {
		name = "MEDIUM_GUN_09"
		template = "medium_turret"
		locatorname = "root"
	}
	component_slot = {
		name = "MEDIUM_GUN_10"
		template = "medium_turret"
		locatorname = "root"
	}
	
	small_utility_slots = 1
	medium_utility_slots = 1
	large_utility_slots = 1
	aux_utility_slots = 10
}
